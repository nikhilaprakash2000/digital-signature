<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Convert PDF to JPEG</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    
    <style>
    body{ font-family:Tahoma, Geneva, sans-serif; color:#000; font-size:11px; background-color:white; margin: 5vmin; padding:0;}
    </style>
    <style type="text/css">
    #upload-form {
        padding: 40px;
        background: rgb(219, 205, 205);
        border: 2px dashed #000;
        margin-left: auto;
        margin-right: auto;
        width: 400px;
    }
    #upload-form input[type=file] {
      
        border: 1px solid #000;
        padding: 4px;
    }
    #upload-form input[type=submit] {
        height: 30px;
    }
</style>
    
    <style type="text/css">    
      img {border-width: 0}
      * {font-family:'Lucida Grande', sans-serif;}
    </style>
  </head>
  <body>
    
<?php
$message = "";
$display = "";
if($_FILES)
{    
    $output_dir = "uploads/";    
    ini_set("display_errors",1);
    if(isset($_FILES["myfile"]))
    {
        $RandomNum   = time();
        
        $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name']));
        $ImageType      = $_FILES['myfile']['type']; //"image/png", image/jpeg etc.
     
        $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
        $ImageExt       = str_replace('.','',$ImageExt);
        if($ImageExt != "pdf")
        {
            $message = "Invalid file format only <b>\"PDF\"</b> allowed.";
        }
        else
        {
            $ImageName      = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt;         
            move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $NewImageName);        
            $location= __DIR__;
            $name       = $output_dir. $NewImageName;
            $num = count_pages($name);
            $RandomNum   = time();
            $nameto     = $output_dir.$RandomNum.".jpg";        
            $location . " " .  $convert    = $location . " " . $name . " ".$nameto;
            exec("convert ".$convert);
            for($i = 0; $i<$num;$i++)
            {
              $display .= "<img src='$output_dir$RandomNum-$i.jpg' title='Page-$i' /><br>";            
            }
            $message = "PDF converted to JPEG sucessfully!!";
        }
    }
}
    function count_pages($pdfname) {
          $pdftext = file_get_contents($pdfname);
          $num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
          return $num;
        }
    
    $content ='<br><form enctype="multipart/form-data"  id="upload-form" action="up.php"  action="" method="post">
<h2>Upload PDF</h2><br />
 Select PDF File: <input name="myfile" type="file" /><br /><br />
 <input type="submit" value="Upload" />
 </form> <h2>'.$message.'</h2><br />'.$display.'';
   
echo $content;
?>
</body>
</html>